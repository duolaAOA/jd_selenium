#-*-coding:utf-8-*-


from selenium import webdriver
import time
from datetime import datetime
import re
import requests
import json
import random
from lxml import etree

from data.save_data import get_Mysql
from data.user_agents import agents

class JD_phone(object):
    def __init__(self,max_page,dbname,company,user_name):
        #集合去重
        self.set = set()
        #设置最大爬取页数
        self.max_page = max_page
        self.T = datetime.strftime(datetime.now(),'%Y%m%d%H%M')
        self.headers = {"user-agent":random.choice(agents)}
        self.base_url = 'https://global.jd.com/'
        self.key = '手机'
        self.dbname = dbname
        self.user_name = user_name
        self.company = company
        self.mysql = get_Mysql(dbname,company,user_name)
        self.mysql.create_table()

        try:
            self.chrome_opt = webdriver.ChromeOptions()
            # 设置chromedriver不加载图片
            self.prefs = {"profile.managed_default_content_settings.images": 2}
            self.chrome_opt.add_experimental_option("prefs", self.prefs)
            self.driver = webdriver.Chrome(chrome_options=self.chrome_opt,executable_path='E:/video course/Python/py3.5.2/Lib/site-packages/selenium/webdriver/chromedriver.exe')
            # self.driver = webdriver.Chrome(executable_path='E:/video course/Python/py3.5.2/Lib/site-packages/selenium/webdriver/chromedriver.exe')
            # self.driver = webdriver.PhantomJS(executable_path='E:/video course\Python/py3.5.2/Lib\site-packages/selenium/webdriver/phantomjs/phantomjs-2.1.1-windows/bin/phantomjs.exe')
            self.driver.maximize_window()
            self.driver.get(self.base_url)
            # time.sleep(0.2)
            self.driver.find_element_by_id("key").clear()
            self.driver.find_element_by_id("key").send_keys(self.key)
            self.driver.find_element_by_tag_name("button").click()

        except Exception as e:
            print(e)


    def save_info(self,*args):
        '''解析每个单品页面，接受2个参数，分别是商品ID和价格'''
        url = "https://item.jd.com/{}.html".format(args[0])
        html = requests.get(url,headers=self.headers).text
        tree = etree.HTML(html)
        item = {}
        item['id'] = args[0]
        item['price'] = args[1]
        is_jd = tree.xpath("//em[@class='u-jd']/text()")
        if is_jd:
            item['is_jd'] = is_jd[-1].strip()
        else:
            item['is_jd'] = ''

        shopname = tree.xpath("//div[@class='name']/a/@title|//div[@class='shopName']/strong/span/a/text()")
        if shopname:
            item['shopname'] = shopname[0]
        else:
            item['shopname'] = ''
        brand = tree.xpath(
            "//dt[text()='品牌']/following-sibling::*[1]/text()|//dt[text()='品牌']/following-sibling::*[1]/text()")

        if brand:
            item['brand'] = brand[0]
        else:
            item['brand'] = ''
        year = tree.xpath(
            "//dt[text()='上市年份']/following-sibling::*[1]/text()|//td[text()='上市年份']/following-sibling::*[1]/text()")
        if year:
            item['years'] = year[0]
        else:
            item['years'] = ''

        month = tree.xpath(
            "//dt[text()='上市月份']/following-sibling::*[1]/text()|//td[text()='上市月份']/following-sibling::*[1]/text()")
        if month:
            item['months'] = month[0]
        else:
            item['months'] = ''

        weight = tree.xpath(
            "//dt[text()='机身重量（g）']/following-sibling::*[1]/text()|//dt[text()='机身重量（g）']/following-sibling::*[1]/text()")
        if weight:
            item['weight'] = weight[0]
        else:
            item['weight'] = ''

        thick = tree.xpath(
            "//dt[text()='机身厚度（mm）']/following-sibling::*[1]/text()|//dt[text()='机身厚度（mm）']/following-sibling::*[1]/text()")
        if thick:
            item['thick'] = thick[0]
        else:
            item['thick'] = ''

        long = tree.xpath(
            "//dt[text()='机身长度（mm）']/following-sibling::*[1]/text()|//dt[text()='机身长度（mm）']/following-sibling::*[1]/text()")
        if long:
            item['longs'] = long[0]
        else:
            item['longs'] = ''

        cpu_brand = tree.xpath(
            "//dt[text()='CPU品牌']/following-sibling::*[1]/text()|//dt[text()='CPU品牌']/following-sibling::*[1]/text()")
        if cpu_brand:
            item['cpu_brand'] = cpu_brand[0]
        else:
            item['cpu_brand'] = ''

        cpu_num = tree.xpath(
            "//dt[text()='CPU核数']/following-sibling::*[1]/text()|//dt[text()='CPU核数']/following-sibling::*[1]/text()")
        if cpu_num:
            item['cpu_num'] = cpu_num[0]
        else:
            item['cpu_num'] = ''

        sim_num = tree.xpath(
            "//dt[text()='双卡机类型']/following-sibling::*[1]/text()|//dt[text()='双卡机类型']/following-sibling::*[1]/text()")
        if sim_num:
            item['sim_num'] = sim_num[0]
        else:
            item['sim_num'] = ''

        sim = tree.xpath('//*[@id="detail"]/div[2]/div[2]/div[1]/div[5]/dl/dd[4]/text()')
        if sim:
            item['sim'] = sim[0]
        else:
            item['sim'] = ''

        rom = tree.xpath('//*[@id="detail"]/div[2]/div[2]/div[1]/div[6]/dl/dd[2]/text()')
        if rom:
            item['rom'] = rom[0]
        else:
            item['rom'] = ''

        ram = tree.xpath( '//*[@id="detail"]/div[2]/div[2]/div[1]/div[6]/dl/dd[4]/text()')
        if ram:
            item['ram'] = ram[0]
        else:
            item['ram'] = ''

        size = tree.xpath(
            "//dt[text()='主屏幕尺寸（英寸）']/following-sibling::*[1]/text()|//dt[text()='主屏幕尺寸（英寸）']/following-sibling::*[1]/text()")
        if size:
            item['sizes'] = size[0]
        else:
            item['sizes'] = ''

        front_c = tree.xpath(
            "//dt[text()='前置摄像头']/following-sibling::*[1]/text()|//dt[text()='前置摄像头']/following-sibling::*[1]/text()")
        if front_c:
            item['front_c'] = front_c[0]
        else:
            item['front_c'] = ''

        back_c = tree.xpath(
            "//dt[text()='后置摄像头']/following-sibling::*[1]/text()|//dt[text()='后置摄像头']/following-sibling::*[1]/text()")
        if back_c:
            item['back_c'] = back_c[0]
        else:
            item['back_c'] = ''

        battery = tree.xpath(
            "//dt[text()='电池容量（mAh）']/following-sibling::*[1]/text()|//dt[text()='电池容量（mAh）']/following-sibling::*[1]/text()")
        if battery:
            item['battery'] = battery[0]
        else:
            item['battery'] = ''

        dsr = self.get_dsr(args[0])['CommentsCount'][0]
        item['AverageScore'] = dsr['AverageScore']#平均分
        item['GoodRateShow'] = dsr['GoodRateShow']#好评率
        item['total_com'] = dsr['CommentCount']#全部评价
        item['good_com'] = dsr['GoodCount']#好评数
        item['mid_com'] = dsr['GeneralCount']
        item['bad_com'] = dsr['PoorCount']
        item['good_lv'] = dsr['GoodRate']
        item['mid_lv'] = dsr['GeneralRate']
        item['bad_lv'] = dsr['PoorRate']
        print(item)

        self.mysql.insert(data=item)
    def get_dsr(self, id):
        '''提取dsr'''
        url = 'https://club.jd.com/comment/productCommentSummaries.action?referenceIds={}'.format(id)
        html = requests.get(url, headers=self.headers).text
        data = json.loads(html)
        return data


    def get_all(self):
        '''提取页面的ID，并按照每个ID去解析单个商品的页面提取信息,递归翻页'''
        time.sleep(0.1)
        '''将网页滑到最下面，加载出所有的商品，这个滑动的次数可以根据试探来选择最少次数和最短等待时间'''
        for i in range(18):
            js = "window.scrollTo(0,{})".format(i * 500)
            self.driver.execute_script(js)
        #     time.sleep(0.2)
        # self.driver.save_screenshot('screen.png')

        #将加载后的网页解析出来

        tree = etree.HTML(self.driver.page_source)
        result = tree.xpath('//div[@id="J_goodsList"]/ul/li')
        if result:
            for each in result:
                try:
                    id = each.xpath('@data-sku')[0]
                    price = each.xpath('*/div/strong/i/text()')[0]
                    #price = each.cssselect('div.p-price > strong > i')[0].text
                except Exception as e:
                    print(e)
                if id not in self.set:
                    try:
                        self.save_info(id,price)
                        self.set.add(id)
                    except Exception as e:
                        print(id, e)

        next_page = tree.xpath('//*[@id="J_bottomPage"]/span[1]/a[9]')[0]
        if next_page and self.max_page - 1:
            self.max_page -= 1
            self.driver.find_element_by_xpath('//*[@id="J_bottomPage"]/span[1]/a[9]').click()
            time.sleep(0.1)
            next_url = self.driver.current_url
            pagenum = re.findall("&page=(\d*?)&", next_url)[0]
            pagenum = (int(pagenum) + 1) // 2
            print('找到下一页，即将提取第{}页的信息'.format(pagenum))
            self.get_all()
        else:
            print("所有页面已经全部提取完毕！")
            self.mysql.close_table()

if __name__ == '__main__':
    jd = JD_phone(101,'e-commerce','JD','phone') #传入需要爬取的页数
    jd.get_all()






























